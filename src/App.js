import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import AddProduct from './pages/AddProduct.js';
import Error from './pages/Error.js';
import Home from './pages/Home';
import Products from './pages/Products';
import ViewProduct from './pages/ViewProduct';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Profile from './pages/Profile';
import Register from './pages/Register';
import './App.css';
import { UserProvider } from './UserContext';

function App() {


  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })


  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])


  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token') }`
      }
    })
    .then(response => response.json())
    .then(data => {
      console.log(data);

        if(typeof data._id !== "undefined") {
           setUser({
              id: data._id,
              isAdmin: data.isAdmin
           });
        } else {
          setUser({
            id: null,
            isAdmin: null
          });
        }
    })
  }, [])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <Container fluid className="webBG min-vh-100">
            <AppNavbar/>
            <Routes>
                <Route path="/" element={<Home />} /> 
                <Route path="/products" element={<Products />} /> 
                <Route path="/products/:productId" element={<ViewProduct />} /> 
                <Route path="/login" element={<Login />} /> 
                <Route path="/logout" element={<Logout />} /> 
                <Route path="/profile" element={<Profile />} /> 
                <Route path="/register" element={<Register />} /> 
                <Route path="/addProduct" element={<AddProduct />} /> 
                <Route path="*" element={<Error />} />
            </Routes>
        </Container>
      </Router>
    </UserProvider>
    
  );
}

export default App;
