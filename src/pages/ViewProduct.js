import { useState, useEffect, useContext } from 'react';
import { Container, Card, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import CheckoutOrder from '../components/CheckoutOrder';
import UserContext from '../UserContext';


export default function ViewProduct() {

	const { user } = useContext(UserContext)
	const { productId } = useParams();

	//States
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0);

	const navigate = useNavigate();
	const peso = '\u20B1';


	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`)
		.then(response => response.json())
		.then(data => {
			console.log(data);

			if(data === false || user.isAdmin === true){
				navigate("*");
			}

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		})

	}, [productId, navigate, user.isAdmin]);

	return (

		<Container className="mt-5">
			<Row>
				<Col lg={{span: 6, offset: 3}}>
					<Card className="borderHighlight">
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>

							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text> 

							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{peso} {price}</Card.Text>

							{
								user.id !== null ?
									<CheckoutOrder product={productId} />
									:
									<Link className="btn btn-danger" to="/login">Login to Order</Link>
							}
						    
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}