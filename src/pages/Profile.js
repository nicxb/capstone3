import { useContext, useState, useEffect } from 'react';
import { Row, Col } from 'react-bootstrap';
import ResetPassword from '../components/ResetPassword';
import ViewOrders from '../components/ViewOrders';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';


export default function Profile(){

    const { user } = useContext(UserContext);

    const [ details, setDetails ] = useState({})

    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
           
            if (typeof data._id !== "undefined") {

                setDetails(data);

            }
        });
    };

    useEffect(() => {
        fetchData();
    }, []);


    return (
        (user.id === null) ?
        <Navigate to="/" />
        :
        <>
            <Row>
                <Col className="p-5 m-5 borderHighlight">
                    <h1 className="my-1 text-center">{`${details.firstName} ${details.lastName}`}</h1>
                    <h4 className="mt-3">My Profile</h4>
                    <hr />
                    <h4>Contact Information</h4>
                    <ul>
                        <li>Email: {details.email}</li>
                        <li>Mobile No: {details.mobileNo}</li>
                    </ul>
                </Col>
            </Row>

            <Row className="pt-4 mt-4">
                <Col>
                    <ViewOrders />
                </Col>
            </Row>

            <Row className="pt-4 mt-4">
                <Col>
                    <ResetPassword/>

                </Col>
            </Row>
        </>


    )

}
