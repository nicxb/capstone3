import Banner from '../components/Banner.js';
import FeaturedProducts from '../components/FeaturedProducts.js';
import logoImage from '../images/logo.png';


export default function Home(){

	const data = {
	   
	    content: "A shop for all of your shining-shimmering-splendid PC needs!",
	    destination: "/products",
	    label: "Shop now!"
	}

	return(
		<>
			<div id="home-page" className="d-flex flex-column align-items-center">
				<div className="borderHighlight m-5 col-12 col-md-10 text-center">
					<img className="imgStyle" src={logoImage} alt="Our Logo" />
					<Banner data={data}/>
				</div>
				
				<FeaturedProducts/>
			</div>
			
		</>
	)
}