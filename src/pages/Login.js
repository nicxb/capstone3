import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {


    const { user, setUser } = useContext(UserContext);

    // input states
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);


    function authenticate(e) {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/api/users/login`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(response => response.json())
        .then(data => {
            console.log(data.accessToken)

            if(data.accessToken) {
        
                localStorage.setItem('token', data.accessToken);

                retrieveUserDetails(data.accessToken)

                Swal.fire({
                    title: "Login successful",
                    icon: "success",
                    text: "Welcome to Paningning's PC Essentials"
                })

            } else {
                
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again."
                })
            }
        })
        
        setEmail('');
        setPassword('');
    };


    const retrieveUserDetails = (token) => {

        fetch(`${process.env.REACT_APP_API_URL}/api/users/details`, {
            headers: {
                Authorization: `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            });

        })
    };



    useEffect(() => {

        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    return (    
            
        (user.id !== null) ? 
            <Navigate to="/products" />

        :
            <div className="d-flex flex-column align-items-center">
                <div className="borderStyle p-4 col-12 col-md-6">
                    <Form onSubmit={(e) => authenticate(e)} >
                        <h1 className="my-5 text-center">Login</h1>
                        <Form.Group controlId="userEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                                type="email" 
                                placeholder="Enter email"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                                type="password" 
                                placeholder="Password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <div className="text-center">
                            <Button className="buttonStyle" variant="success" type="submit" disabled={isActive === false}>Login</Button>
                        </div>      
                    </Form>
                                    
                    <p className="text-center pt-3">
                        Don't have an account yet? <a className="link" href="/register">Click here</a> to register.
                    </p>
                </div>
            </div>
            
              
     
    )
}