import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	// input states
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	// Button state
	const [isActive, setIsActive] = useState(false);

	function registerUser(event){

		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/api/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		}).then(response => response.json()).then(result => {
			if(result){
				setFirstName("")
				setLastName("")
				setEmail("")
				setMobileNo("")
				setPassword("")
				setConfirmPassword("")

				Swal.fire({
				    title: "Registration successful",
				    icon: "success",
				    text: "You may now log in to your account."
				}).then(result => {
					if(result.isConfirmed) {
						navigate("/login");
					}
				})

			} else {
				Swal.fire({
				    title: "Registration failed",
				    icon: "error",
				    text: "Something went wrong, please try again."
				})
			}
		})
	}

	useEffect(() => {
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, email, mobileNo, password, confirmPassword]);

	return(
		(user.id !== null) ?
			<Navigate to="/products" />
		:
			<div className="d-flex flex-column align-items-center">
				<div id="register" className="borderStyle p-4 col-12 col-md-6">
					<Form className="d-flex flex-column grid gap-3 p-3" onSubmit={(event) => registerUser(event)}>
			        	<h1 className="my-5 text-center">Register</h1>
			            <Form.Group>

			                <Form.Control
			                	type="text" 
			                	placeholder="Enter First Name" 
			                	required
			                	value={firstName}
			                	onChange={event => {setFirstName(event.target.value)}}
			                />
			            </Form.Group>
			            <Form.Group>
			                
			                <Form.Control 
			                	type="text" 
			                	placeholder="Enter Last Name" 
			                	required
			                	value={lastName}
			                	onChange={event => {setLastName(event.target.value)}}
			                />
			            </Form.Group>
			            <Form.Group>
			                
			                <Form.Control 
			                	type="email" 
			                	placeholder="Enter Email" 
			                	required
			                	value={email}
			                	onChange={event => {setEmail(event.target.value)}}
			                />
			            </Form.Group>
			            <Form.Group>
			                
			                <Form.Control 
			                	type="number" 
			                	placeholder="Enter 11 Digit No." 
			                	required
			                	value={mobileNo}
			                	onChange={event => {setMobileNo(event.target.value)}}
			                />
			            </Form.Group>
			            <Form.Group>
			                
			                <Form.Control 
			                	type="password" 
			                	placeholder="Enter Password" 
			                	required
			                	value={password}
			                	onChange={event => {setPassword(event.target.value)}}
			                />
			            </Form.Group>
			            <Form.Group>
			                
			                <Form.Control 
			                	type="password" 
			                	placeholder="Confirm Password" 
			                	required
			                	value={confirmPassword}
			                	onChange={event => {setConfirmPassword(event.target.value)}}
			                />
			            </Form.Group>

			            <div className="text-center">
			            	<Button className="buttonStyle" variant="success" type="submit" disabled={isActive === false}>Submit</Button>
			            </div>
			                  
			       </Form>

			       <p className="text-center pt-3">
			       		Already have an account? <a className="link" href="/login">Click here</a> to Login.
			       </p>
				</div>
			</div>
			
				
	)
}