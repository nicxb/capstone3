import { useState } from 'react';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function SetAdmin({ user, isAdmin, fetchData }) {

	const [ userId ] = useState(user);


	const setAdminToggle = () => {
		fetch(`${process.env.REACT_APP_API_URL}/api/users/${userId}/set-admin`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
			
			if(data === true){
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'User has been set as Administrator successfully'
				})

				fetchData();

			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})
	}

	const setNonAdminToggle = () => {
		fetch(`${process.env.REACT_APP_API_URL}/api/users/${userId}/set-non-admin`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
			
			if(data === true){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'User has been set as Non-Administrator'
				})

				fetchData();

			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})
	}




	return(
		<>
			{(isAdmin === false) ? 
				<Link className="btn btn-danger" size="sm" onClick={setAdminToggle}>Set Admin</Link>
			:
				<Link className="btn btn-warning" size="sm" onClick={setNonAdminToggle}>Set non-Admin</Link>
			}

		</>
	)
}