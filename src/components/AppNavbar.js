import { useContext } from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import logoIcon from '../images/logoIcon.png';

export default function AppNavbar(){

	const { user } = useContext(UserContext);
	console.log(user);

	return (
		<Navbar id="appNavBar"  expand="lg">
			<Container fluid>
			    <Navbar.Brand as={Link} to="/">
			    	<img src={logoIcon} alt="logo"/>
			    	Paningning PC Essentials
			    </Navbar.Brand>
			    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
			    <Navbar.Collapse id="basic-navbar-nav">
			    	<Nav className="ms-auto">
			    		<Nav.Link className="fw-bold" as={NavLink} to="/">Home</Nav.Link>

			    		{(user.isAdmin === true) ?
			    			<Nav.Link className="fw-bold" as={NavLink} to="/products">Admin Dashboard</Nav.Link>
			    			:
			    			<Nav.Link className="fw-bold" as={NavLink} to="/products">Products</Nav.Link>
			    		}

			    		{(user.id !== null) ?
			    				user.isAdmin ?
			    					<>
			    						<Nav.Link className="fw-bold" as={NavLink} to="/logout">Logout</Nav.Link>
			    					</>
			    					:
			    					<>
			    						<Nav.Link className="fw-bold" as={NavLink} to="/profile">Profile</Nav.Link>
			    						<Nav.Link className="fw-bold" as={NavLink} to="/logout">Logout</Nav.Link>
			    					</>
			    			:
			    				<>
			    					<Nav.Link className="fw-bold" as={NavLink} to="/login">Login</Nav.Link>
			    					<Nav.Link className="fw-bold" as={NavLink} to="/register">Register</Nav.Link>
			    				</>
			    		}

			    	</Nav>
			    </Navbar.Collapse>
			</Container>
		</Navbar>
	)
}