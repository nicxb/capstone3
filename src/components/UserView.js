import { useState, useEffect } from 'react';
import ProductCard from './ProductCard';


export default function UserView({ productsData }) {

	const [products, setProducts] = useState([]);

	useEffect(() => {
		const productsArr = productsData.map(product => {
			if(product.isActive === true) {
				return (
					<ProductCard productProp={product} key={product._id} />
				)
			} else {
				return null;
			}
		})

		setProducts(productsArr);
	}, [productsData])


	return (

		<>
			<h1 className="text-center text-light m-3">Products</h1>
			<div className="d-flex flex-wrap grid gap-4 justify-content-center">
				{ products }
			</div>
			
		</>
	)

}