import { useState, useEffect, useContext } from 'react';
import { Row, Accordion } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function ViewOrders() {

	const [ orders, setOrders ] = useState([]);
	const { user } = useContext(UserContext);
	const userId = user.id;
	const peso = '\u20B1';

	console.log(user.id);

	useEffect(() => {
		
		fetch(`${process.env.REACT_APP_API_URL}/api/orders/${userId}`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
		}).then(response => response.json())
		.then(data => {
			console.log(data);
			const ordersArr = data.map(order => {
				const products = order.products;
				console.log(products);

	            return (
	            	
				        <Accordion.Item className="orderlist" key={order._id} eventKey={order._id}>
				          <Accordion.Header>Order # purchased on {order.purchasedOn}</Accordion.Header>
				          <Accordion.Body>
				            Product: {products[0].productName}
				            <br/>
				            Quantity: {products[0].quantity}
				            <hr/>
				            Total Amount: {peso} {order.totalAmount}				            
				          </Accordion.Body>
				        </Accordion.Item>
				   
	                
	            )
	        })

			setOrders(ordersArr);
		})

		

	}, [ userId ])

	return (

		<>	
			<div id="order-history" className="borderHighlight p-5">
				<Row>
					<h1 className="text-center">My Orders</h1>
					<Accordion>
						{ orders }
					</Accordion>
				</Row>
			</div>
			
		</>
	)

}