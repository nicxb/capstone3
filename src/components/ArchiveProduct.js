import { useState } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function ArchiveProduct({ product, fetchData, isActive }) {

	const [ productId ] = useState(product);

	const archiveToggle = () => {
		fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}/archive`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
			
			if(data === true){
				Swal.fire({
					title: 'Archived!',
					icon: 'success',
					text: 'Product archived successfully'
				})

				fetchData();

			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})
	}


	const activateToggle = () => {
		fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}/activate`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
			
			if(data === true){
				Swal.fire({
					title: 'Activated!',
					icon: 'success',
					text: 'Product activated successfully'
				})

				fetchData();

			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})
	}




	return(
		<>
			{(isActive === true) ? 
				<Button variant="danger" size="sm" onClick={archiveToggle}>Archive</Button>
			:
				<Button variant="success" size="sm" onClick={activateToggle}>Activate</Button>
			}
		</>
	)
}