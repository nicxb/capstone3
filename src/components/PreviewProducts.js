import { Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function PreviewProduct(props) {

	const { breakPoint, data } = props;
	const { _id, name, description, price } = data;
	const peso = '\u20B1';


	return (
		<Col xs={12} md={breakPoint}>
			<Card className="cardHighlight mx-2 mb-4 text-light">
				<Card.Body className="text-center">
					<Card.Title>
						<Link id="featuredNames" to={`/products/${_id}`}>{name}</Link>
					</Card.Title>
					<Card.Text>
						{description}
					</Card.Text>
				</Card.Body>
				
				<Card.Footer>
					<h5 className="text-center txt-price">{peso} {price}</h5>
					<Link className="btn btn-success d-block" to={`/products/${_id}`}>Details</Link>
				</Card.Footer>
			</Card>
		</Col>
	)
}