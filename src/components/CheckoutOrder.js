import { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import {  useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function CheckoutOrder({ product }) {

	const [productId, setProductId] = useState('');
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [quantity, setQuantity] = useState(1);
		// useState for the checkout Modal
	const [showCheckout, setShowCheckout] = useState(false);

	const navigate = useNavigate();
	const peso = '\u20B1';
	const totalPrice = price * quantity;

	const openCheckout = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`)
		.then(response => response.json())
		.then(data => {

			setProductId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)
		})

		setShowCheckout(true);
	}

	const closeCheckout = () => {
		setShowCheckout(false);
		setName('')
		setDescription('');
		setPrice('');
	}


	const checkout = (e, productId) => {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/api/orders/checkout`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				productName: name,
				quantity: quantity
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
				    title: "Successfully ordered",
				    icon: "success",
				    text: "You have successfully ordered this product."
				})

				navigate("/products");

			} else {
				Swal.fire({
				    title: "Something went wrong",
				    icon: "error",
				    text: "Please try again."
				})
			}
		})
	};

	return(
		<>
			<Button variant="success" size="lg" onClick={() => openCheckout(product)}>Checkout</Button>

			<Modal show={showCheckout} onHide={closeCheckout}>
				<Form onSubmit={e => checkout(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Checkout</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<p>{name}</p>
						<p>{description}</p>
						<p>{peso} {price}</p>

						<Form.Group controlId="productQuantity">
							<Form.Label>Quantity</Form.Label>
							<Form.Control 
								type="number" 
								value={quantity}
								min="1"
								onChange={e => setQuantity(e.target.value)}
								required
							/>
						</Form.Group>

						<h5>Total Amount</h5>
						<h5>{peso} {totalPrice}</h5>

					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeCheckout}>Close</Button>
						<Button variant="success" type="submit">Checkout</Button>
					</Modal.Footer>
				</Form>
			</Modal>

		</>
	)
}