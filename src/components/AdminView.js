import { useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import SetAdmin from './SetAdmin';
import UpdateProduct from './UpdateProduct';
import ArchiveProduct from './ArchiveProduct';

export default function AdminView({ productsData, fetchData }) {

    const [ products, setProducts ] = useState([])
    // Adding user list functionality
    const [ isProductTable, setIsProductTable ] = useState(true);
    const [ users, setUsers ] = useState();

    const showUsers = () => {
        setIsProductTable(false);
    }

    const showProducts = () => {
        setIsProductTable(true);
    }

    // useEffect for the products array table
    useEffect(() => {
        const productsArr = productsData.map(product => {
            return (
                <tr key={product._id}>
                    <td>{product._id}</td>
                    <td>{product.name}</td>
                    <td>{product.description}</td>
                    <td>PHP {product.price}</td>
                    <td className={product.isActive ? "text-success" : "text-danger"}>
                        {product.isActive ? "Available" : "Unavailable"}
                    </td>
                    <td><UpdateProduct product={product._id} fetchData={fetchData} /></td>        
                    <td><ArchiveProduct 
                            product={product._id} 
                            fetchData={fetchData} 
                            isActive={product.isActive}
                        />
                    </td>               
                </tr>
            )
        })

        setProducts(productsArr)
    }, [ productsData, fetchData ])


    useEffect(() => {

        fetch(`${process.env.REACT_APP_API_URL}/api/users/all`, {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(data => {
            const usersArr = data.map(user => {

                return(
                    <tr key={user._id} className="text-center">

                        <td>{user.firstName} {user.lastName}</td>
                        <td>{user.email}</td>
                        <td className={user.isAdmin ? "text-success" : "text-danger"}>
                            {user.isAdmin ? "Admin" : "non-Admin"}
                        </td>
                        <td><SetAdmin 
                            user={user._id} 
                            fetchData={fetchData} 
                            isAdmin={user.isAdmin}
                            />
                        </td>        
                    </tr>
                )
            })

            setUsers(usersArr);
        })

    }, [ fetchData ]);


    return(
        <>
            <div className="borderHighlight m-5">
                <h1 className="text-center my-4"> Admin Dashboard</h1>
                <div className="d-flex justify-content-center mb-3 grid gap-2">
                    <Link className="btn btn-warning" to='/addProduct'>Add New Product</Link>

                    { isProductTable ?
                            <Button variant="success" onClick={showUsers}>Show Users</Button>
                        :
                            <Button variant="secondary" onClick={showProducts}>Show Products</Button>
                    }

                    
                </div>
                
                { isProductTable ?
                        <div id="admin_products" className="mx-4">
                            <Table striped bordered hover responsive>
                                <thead className="tableHead">
                                    <tr className="text-center">
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th className="dt-responsive">Price</th>
                                        <th>Availability</th>
                                        <th colSpan="2">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    { products }
                                </tbody>
                            </Table>  
                        </div>
                    :
                        <div id="admin_user" className="mx-4">
                            <Table striped bordered hover responsive>
                                <thead className="tableHead">
                                    <tr className="text-center">

                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Admin status</th>
                                        <th colSpan="2">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    { users }
                                </tbody>
                            </Table>  
                        </div>
                }
            </div>
            
        </>

        )
}