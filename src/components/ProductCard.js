import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default function ProductCard({productProp}){

	const {name, description, price, _id} = productProp;
	const peso = '\u20B1';

	return(
		<Card className="my-2 text-center cardHighlight text-light col-12 col-md-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>

				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text> 

				
			</Card.Body>
			<Card.Footer >
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text className="txt-price">{peso} {price}</Card.Text>

			    <Link className="btn btn-success" to={`/products/${_id}`}>Details</Link>
			</Card.Footer>
		</Card>
	)
}

// PropTypes is used for validating the data from the props
ProductCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}