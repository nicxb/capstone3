import { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function UpdateProduct({ product, fetchData }) {

	const [productId, setProductId] = useState('');
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	// useState for the Update Modal
	const [showUpdate, setShowUpdate] = useState(false);


	const openUpdate = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setProductId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)
		})

		setShowUpdate(true);
	}

	const closeUpdate = () => {
		setShowUpdate(false);
		setName('')
		setDescription('');
		setPrice(0);
	}

	//function for updating the product
	const updateProduct = (e, productId) => {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/api/products/${productId}`,{
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data === true) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product updated successfully.'
				})
				closeUpdate();
				fetchData()
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again.'
				})
				closeUpdate();
			}
		})

	};


	return(
		<>
			<Button variant="warning" size="sm" onClick={() => openUpdate(product)}>Update</Button>

			<Modal show={showUpdate} onHide={closeUpdate}>
				<Form onSubmit={e => updateProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Update Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control 
								type="text"
								value={name}
								onChange={e => setName(e.target.value)} 
								required
							/>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control 
								type="text"
								value={description}
								onChange={e => setDescription(e.target.value)} 
								required
							/>
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control 
								type="number" 
								value={price}
								onChange={e => setPrice(e.target.value)}
								required
							/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeUpdate}>Close</Button>
						<Button variant="success" type="submit">Update</Button>
					</Modal.Footer>
				</Form>
			</Modal>

		</>
	)
}